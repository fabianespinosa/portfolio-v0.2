var $j = jQuery.noConflict();

var leftTop = 'img/f06-860.gif';
var leftBottom = 'img/f03-860.gif';
var rightTop = 'img/f04-860.gif';
var rightBottom = 'img/f05-860.gif';
var middleTop = 'img/f07-860.gif';
var middleRightTop = 'img/f02-860.gif';
var middleBottom = 'img/f01-860.gif';

var WindowsSizeWidth=function(){
	var w=$j(window).width();
	return w;
};
var WindowsSizeHeigth=function(){
	var h=$j(window).height();
	return h;
};



$j(function(){
	
	$j(document).mousemove(function(event) {
		var percentX = event.pageX / WindowsSizeWidth() * 100;
		var percentY = event.pageY / WindowsSizeHeigth() * 100;


		if (WindowsSizeWidth() <= 937) {
			if (percentY < 50) {
				// top
				if (percentX < 30) {
					// $j(".me").attr('src', leftTop);					
					document.querySelector('.me').setAttribute('src', leftTop);
				} else if (percentX >= 30 && percentX <= 50) {
					// $j(".me").attr('src', middleTop);
					document.querySelector('.me').setAttribute('src', rightTop);
				} else if (percentX > 50 && percentX <= 74) {
					// $j(".me").attr('src', middleRightTop);
					document.querySelector('.me').setAttribute('src', middleRightTop);
				} else { 
					// $j(".me").attr('src', rightTop);
					document.querySelector('.me').setAttribute('src', middleTop);
				}
			} else {
				// bottom
				if (percentX < 33) {
					// $j(".me").attr('src', leftBottom);
					document.querySelector('.me').setAttribute('src', leftBottom);
				} else if  (percentX > 65){
					// $j(".me").attr('src', rightBottom);
					document.querySelector('.me').setAttribute('src', rightBottom);
				} else {
					// $j(".me").attr('src', middleBottom);
					document.querySelector('.me').setAttribute('src', middleBottom);
				}
			}
		} else {
			// document.querySelector('h2.test').innerHTML = 'WindowsSizeWidth > 937';
			if (percentY < 70) {
				// top
				if (percentX < 30) {
					// $j(".me").attr('src', leftTop);
					document.querySelector('.me').setAttribute('src', leftTop);
				} else if  (percentX > 60){
					// $j(".me").attr('src', rightTop);
					document.querySelector('.me').setAttribute('src', rightTop);
				} else {
					// $j(".me").attr('src', middleTop);
					document.querySelector('.me').setAttribute('src', middleTop);
				}
			} else {
				// bottom
				if (percentX < 60) {
					// $j(".me").attr('src', leftBottom);
					document.querySelector('.me').setAttribute('src', leftBottom);
				} else if  (percentX > 82){
					// $j(".me").attr('src', rightBottom);
					document.querySelector('.me').setAttribute('src', rightBottom);
				} else {
					// $j(".me").attr('src', middleBottom);
					document.querySelector('.me').setAttribute('src', middleBottom);
				}
			}
		}

	});
	
	$j(".me").click(function() {
		$j(".site-info").css('padding','2em');
		$j(this).css('display','none');
	});

});