(function(){
	var app = angular.module('portfolio', []);
	app.controller('PortfolioController', function(){
		this.works = works;
	});

	app.directive('works', function(){
		return {
			restrict: 'E',
			templateUrl: 'works.html'
		};
	});

	app.directive('timelineTabs', function(){
		return {
			restrict: 'E',
			templateUrl: 'timeline-tabs.html',
			controller: function(){
				this.tab = 1;
				this.selectTab = function(setTab) {
					this.tab = setTab;
				};
				this.isSelected = function(checkTab) {
					return this.tab === checkTab;
				};
			},
			controllerAs: 'tabs',
		};
	});


	var works = {
		vijftien: [
			{
				name: 'Kleiformance',
				slug: 'klei',
				link: 'http://laferalfang.be',
				date: 'Nov - Jun 2016',
				description: [
					'Kleiformance is de tijdelijke naam van het eindproject van Ester Aragones. Hier experimenteren we met het omgekeerde proces van de keramiek: nadat de potten gemaakt zijn, in plaats van ze te bakken, lossen we ze op in water.',
					'Meer dan een artiestiek project, probeert dit een klein subjectieve onderzoek te zijn. Ester nodigt mensen uit om het ontbinding proces te bekijken en om te praten over de ideeën en gevoelens die getriggerd worden.',
				],
				tags: [
					'videomontage',
					'brainstorming'
				],
				images: [
					'img-gallery/kleiformance.png',
				],
				video: 'https://youtu.be/_aH_LaakaSg',
				isPrivate: false,
			},
			{
				name: 'Het Nadeel van de Twijfel',
				slug: 'nadeel',
				link: 'http://multatuliteater.be/',
				date: 'Dec',
				description: [
					'Eind november maakte ik de affiche van het toneelstuk "Het Nadeel van de Twijfel", voor de Gentse theatergroep Multatuli.',
				],
				tags: [
					'photoshop',
					'grafisch ontwerp',
					'print'
				],
				images: [
					'img-gallery/hnvd-affiche.jpg',
					'img-gallery/hnvd-flyer.jpg',
				],
				video: '',
				isPrivate: false,
				isGallery: true,
			},
			{
				name: 'I Love Mariaberg',
				slug: 'ilm',
				link: 'http://www.facebook.com/ilovemariaberg',
				date: 'Jun - Aug',
				description: [
					'I Love Mariaberg was een sociaal-artiestiek project van Inge Kerfs (Veauxdeville): het leven in Mariaberg door de ogen van de honden van de wijk. In Augustus heb ik de begingeneriek en de montage van het film gemaakt.',
				],
				tags: [
					'videomontage',
					'motion-graphics',
				],
				images: [
					'img-gallery/ilovemariaberg.png',
				],
				video: '',
				isPrivate: false,
				isGallery: false,
			},
			{
				name: 'Victoria',
				slug: 'victoria',
				link: 'https://www.facebook.com/Victoria-202062376791747',
				date: 'Jul',
				description: [
					'Voor het eindproject van Luis Pizarro, de kortfilm Victoria, heb ik een kleine animatie gedaan voor de begingeneriek.',
				],
				tags: [
					'motion-graphics',
				],
				images: [
					'img-gallery/victoria.png',
				],
				video: 'https://youtu.be/ye6rlq0aq_o',
				isPrivate: false,
				isGallery: false,
			},
			{
				name: 'Welzijn Cadans',
				slug: 'cadans',
				link: 'http://welzijncadans.be',
				date: 'Jan - Maa',
				description: [
					'In maart was de website van Welzijn Cadans online. Tussen december 2014 en januari 2015 heben we (ik en Ethel) de mogelijkheden van wordpress besproken. Ik ben begonnen met een wireframe  voor te stellen, gebasseerd op een schets van haar. De tweede stap was een mockup. En nadat we akkoord waren i.v.m. het kleurenpalet en de lay-out, begon ik met de ontwikkeling van een child-theme (op basis van het responsive twenty-fifteen theme).',
				],
				tags: [
					'wordpress',
					'html5',
					'css3',
					'javascript',
					'php',
					'photoshop',
					'illustrator',
				],
				images: [
					'img-gallery/cadans-screenshot.jpg',
					'img-gallery/cadans-mailing.jpg',
				],
				video: '',
				isPrivate: false,
				isGallery: true,
			},
		],
		viertien: [
			{
				name: 'Scratch and Snuffest',
				slug: 'sns',
				link: 'http://www.facebook.com/scratch.andsnuff',
				date: 'Jun-Jul 2014',
				description: [
					'Scratch and Snuffest 2014 was de tweede en laatste editie van een rock festival. Ik heb de oorspronkelijke website aangepast. Ik heb het responsive gemaakt en de stijl afgestemd op die van de affiche.',
				],
				tags: [
					'html5',
					'css3',
					'grafisch ontwerp',
				],
				images: [
					'img-gallery/sns-top.png',
				],
				video: '',
				isPrivate: false,
				isGallery: false,
			},
			{
				name: 'I Love Mariaberg',
				slug: 'ilm2014',
				link: 'http://www.facebook.com/ilovemariaberg',
				date: 'Sep-Okt 2014',
				description: [
					"Tijdens het eerste deel van het social-artistieke project van Inge Kerfs in de Maastrichtse wijk Mariaberg, heb ik de website en de montage van de video's gemaakt.",
				],
				tags: [
					'html5',
					'css3',
					'grafisch ontwerp',
					'videomontage',
				],
				images: [
					'img-gallery/ilm.jpg',
					'img-gallery/browsers-snapshots-ilm03.jpg',
				],
				video: '',
				isPrivate: false,
				isGallery: true,
			},
			{
				name: 'La Piel del Desierto',
				slug: 'lpd',
				link: '',
				date: 'Apr-Dec 2013',
				description: [
					"Luis Ramírez en Christian Palma wilden een boek publiceren met het verslag en de foto's van hun ervaring met het Wixárika volk, toen ik het verhaal hoorde vond ik het project heel aantrekkelijk.",
					"Samen hebben we de strategie voorbereid om een zelf-publicatie te maken. Ik ben verantwoordelijk geweest voor het ontwerp van de website, de posters, postcards en de eerste drafts van het boek. Ik heb ook de laatste montage van de  de promotionele video.",
				],
				tags: [
					'brainstorming',
					'grafisch ontwerp',
					'print',
					'wordpress',
					'html5',
					'css3',
					'javascript',
					'photoshop',
					'indesign',
				],
				images: [
					'img-gallery/lpd-01.jpg',
					'img-gallery/browsers-snapshots-lpd01.jpg',
				],
				video: '',
				isPrivate: false,
				isGallery: true,
			},
			{
				name: 'Rocher Herrera',
				slug: 'rocherart',
				link: '',
				date: 'Dec-Feb 2013',
				description: [
					"Ik heb de wordpress-website en de visite kaartjes van tattoo-artist Rocher Herrera gemaakt.",
				],
				tags: [
					'grafisch ontwerp',
					'print',
					'wordpress',
					'html5',
					'css3',
					'photoshop',
					'illustrator',
				],
				images: [
					'img-gallery/browsers-snapshots-rocher01.jpg',
					'img-gallery/rocher-visitekaart.jpg'
				],
				video: '',
				isPrivate: false,
				isGallery: true,
			},
			{
				name: 'Corneeltjes',
				slug: 'corneeltjes',
				link: '',
				date: 'Sep-Dec 2013',
				description: [
					"Door een php-script aan te passen, heb ik een online geboortelijavascripttsite gemaakt. Ik heb het ontwerp gebaseerd op de geboortekaart.",
				],
				tags: [
					'html5',
					'css3',
					'php',
				],
				images: [
					'img-gallery/k&p.jpg',
				],
				video: '',
				isPrivate: false,
				isGallery: false,
			},
			{
				name: 'Galaksies',
				slug: 'galaksies',
				link: 'http://veauxdecamion.be/',
				date: 'Apr-Dec 2012',
				description: [
					"Eerste samenwerking met Inge Kerfs. Ik heb de volledig montage gemaakt van de verschillende kortfilms. Samen met haar heb ik de kortfilm 'Another Pair of Wings' gemaakt: de montage en de effects heb ik  toegepast. Ik maakte de begingeneriek en  vier animaties gemaakt. En ik deed de montage van de stop motion 'Alieske'.",
				],
				tags: [
					'videomontage',
					'motion-graphics',
					'stop-motion',
					'animatie',
					'video-opname',
				],
				images: [
					'img-gallery/galaksies-01.png',
					'img-gallery/galaksies-02.png',
					'img-gallery/galaksies-03.png',
					'img-gallery/galaksies-04.png',
				],
				video: '',
				isPrivate: false,
				isGallery: false,
			},
			{
				name: 'The Nymph Maniacs',
				slug: 'nymph',
				link: '',
				date: 'Aug-Dec 2011',
				description: [
					"Ik heb de wordpress-website van de vliegvissersclub The Nymph Maniacs ontworpen en ontwikkeld.",
				],
				tags: [
					'wordpress',
					'html',
					'css',
					'grafisch ontwerp',
				],
				images: [
					'img-gallery/tnm-top.jpg',
				],
				video: '',
				isPrivate: false,
				isGallery: false,
			},
		],
		tien: [
			{
				name: 'Uncertainty',
				slug: 'uncertainty',
				link: 'http://danielpastene.tumblr.com/',
				date: 'Jan-Okt 2010',
				description: [
					"Schrijven, productie en realisatie van een fotomontage i.s.m. Daniel Pastene. We wilden een soort homage brengen aan 'La Jettee' van Chris Maker.",
				],
				tags: [
					'brainstorming',
					'video-opname',
				],
				images: [
					'http://36.media.tumblr.com/57806265f204373de3fdf771e2bf550c/tumblr_mona03uzlh1srqya3o2_1280.jpg',
				],
				video: '',
				isPrivate: false,
				isGallery: false,
			},
			{
				name: 'All is already said',
				slug: 'aias',
				link: 'http://youtu.be/uIKVBAI0ti4',
				date: 'Sep-Okt 2009',
				description: [
					"Eerste samenwerking met Daniel Pastene. 'All is already said' is een kortfilm die we gemaakt hedden in het kader van een UN-videowedstrijd (If you had the opportunity to speak to the world leaders, what would you say?)",
				],
				tags: [
					'brainstorming',
					'video-opname',
					'videomontage',
				],
				images: [
					'img-gallery/aias.png',
				],
				video: 'http://youtu.be/uIKVBAI0ti4',
				isPrivate: false,
				isGallery: false,
			},
			{
				name: 'Daniel Pastene',
				slug: 'daniel',
				link: '',
				date: 'Jun-Jul 2008',
				description: [
					"Flash + Html. Website van de Daniel Pastene, componist.",
				],
				tags: [
					'grafisch ontwerp',
					'flash',
					'html',
				],
				images: [
					// 'img-gallery/daniel.jpg',
				],
				video: '',
				isPrivate: true,
				isGallery: false,
			},
			{
				name: 'Atrium Glaskunst',
				slug: 'atrium',
				link: 'http://atriumglaskunst.be',
				date: 'Okt-Dec 2007',
				description: [
					"Flashwebsite van het Glas-in-loodatelier Atrium",
				],
				tags: [
					'grafisch ontwerp',
					'flash',
				],
				images: [
					'img-gallery/atrium.jpg',
				],
				video: '',
				isPrivate: false,
				isGallery: false,

			},
			{
				name: 'La Guerra del Agua',
				slug: 'gagua',
				link: 'http://youtu.be/QFRoQVz1QVs',
				date: '2005',
				description: [
					"In 2005 de argentijnse Pablo Caceres kwam met het idee een animatiekortfilm te maken: 'La Guerra del Agua'. Uiteindelijk is het project nooit ontwikkeld, maar we hebben plezier gehad tijden het maken van deze drie oefeningen.",
					"youtu.be/QFRoQVz1QVs, youtu.be/JqQV2rg-ms0, youtu.be/tsy5E5uuI9s",
				],
				tags: [
					'grafisch ontwerp',
					'brainstorming',
					'flash',
					'animatie',
				],
				images: [
					'img-gallery/lga-01.png',
					'img-gallery/lga-02.png',
					'img-gallery/lga-03.png',
				],
				video: '',
				isPrivate: false,
				isGallery: true,
			},
			{
				name: 'Sórdido Calumnias',
				slug: 'calumnias',
				link: 'http://www.abc.com.py/espectaculos/libros-de-cuentos-y-de-poemas-presentan-hoy-760919.html',
				date: 'Mei 2004',
				description: [
					"Tussen 1999 en 2002 heb ik de verhalen van dit boek geschreven. In 2004, aangedreven door Aníbal Domaniczky heb ik ze uitgewerkt. In Mei van datzelfde jaar hebben we de twee boeken voorgesteld: mijn verhalenbundel en het poeziëboek van Anibal 'El Almacén (tapetujos)'.",
					"Ik heb de layout en de ontwerp van beide boeken gemaakt. Samen hebben we de boeken geassembleerd.",
					"Mijn schoonbroer heeft de voorstelling gefilmd en gemonteerd. Hier is er een stukje daarvan: http://youtu.be/tiNH5hVygO0."
				],
				tags: [
					'schrijven',
					'lay-out',
					'photoshop',
					'page maker',
					'brainstorming',
				],
				images: [
					'img-gallery/sordido_calumnias.jpg',
					'img-gallery/sordido.png',
				],
				video: '',
				isPrivate: false,
				isGallery: true,
			},
		],
		past: [
			{
				name: 'Vroeger',
				slug: 'vroeger',
				link: '',
				date: '',
				description: [
					"Sinds 2001 doe ik mee aan allerlei soort projecten. Wat volgt is een fotogallerij van affiche's, cd-covers en andere...",
				],
				tags: [
					'miscellaneous',
				],
				images: [
					'img-gallery/old/img%20(1).png',
					'img-gallery/cv-timeline.png',
					'img-gallery/old/img%20(3).jpg',
					'img-gallery/old/img%20(4).jpg',
					'img-gallery/old/img%20(1).jpg',
					'img-gallery/old/img%20(2).jpg',
					'img-gallery/old/img%20(9).jpg',
					'img-gallery/old/img%20(10).jpg',
					'img-gallery/old/img%20(11).jpg',
					'img-gallery/old/img%20(12).jpg',
					'img-gallery/old/img%20(13).jpg',
					'img-gallery/old/img%20(14).jpg',
					'img-gallery/old/img%20(15).jpg',
					'img-gallery/old/img%20(5).jpg',
					'img-gallery/old/img%20(8).jpg',
					'img-gallery/old/img%20(16).jpg',
					'img-gallery/old/img%20(7).jpg',
					'img-gallery/old/img%20(6).jpg',
				],
				video: '',
				isPrivate: false,
				isGallery: true,
			}
		]
	};
})();