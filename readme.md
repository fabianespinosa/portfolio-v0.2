# Portfolio v0.2

## Made with:

* nodeJs
* gulp
* stylus
* vanillaJs
* bootstrap

### Mind notes...
`npm init`
`npm install --save-dev gulp`
`npm install gulp-concat gulp-uglify --save-dev`
`npm install jshint gulp-jshint gulp-stylus gulp-rename --save-dev`
`touch gulpfile.js`

`gulp.task` will take 3 arguments:
  task name, task dependency, function calling this task
  for instance: `gulp.task('js', ['css'], function() {` will start `js` after `css` is done.

`gulp` in my `project/portfolio` directory it will start watching my work.

### Some references...
[Getting Started with gulp](https://travismaynard.com/writing/getting-started-with-gulp)
[Gulp-Stylus](https://github.com/stevelacy/gulp-stylus)
[Automatizacion Gulp](https://platzi.com/blog/automatizacion-gulp-js/)